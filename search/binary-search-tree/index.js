'use strict';

class Node {
  constructor(value) {
    this.value = value;
    this.left = null;
    this.right = null;
  }
}

class BinaryTree {
  constructor(value) {
    this.root = value instanceof Node ? value : null;
  }
  add(value) {
    let node = new Node(value);
    if (!this.root) {
      this.root = node;
      return node;
    } else {
      let current = this.root;
      while (true) {
        if (value === current.value) {
          break;
        }
        if (value < current.value) {
          if (!current.left) {
            current.left = node;
            break;
          } else {
            current = current.left;
          }
        } else {
          if (!current.right) {
            current.right = node;
            break;
          } else {
            current = current.right;
          }
        }
      }
    }
    return node;
  }
  contains(value) {
    let found = false;
    let current = this.root;
    while (!found && current) {
      if (value === current.value) {
        found = true;
        break;
      }
      if (value < current.value) {
        current = current.left;
      } else {
        current = current.right;
      }
    }
    return found;
  }
  traverse(cb) {
    function inOrder(node) {
      if (node) {
        if (node.left) {
          inOrder(node.left);
        }
        cb.call(this, node);
        if (node.right) {
          inOrder(node.right);
        }
      }
    }
    inOrder(this.root);
  }
  prefixTraverce(cb) {
    function inOrder(node) {
      if (node) {
        cb.call(this, node);
        if (node.left) {
          inOrder(node.left);
        }
        if (node.right) {
          inOrder(node.right);
        }
      }
    }
    inOrder(this.root);
  }
  remove() {

  }
  size() {
    let length = 0;
    this.traverse(node => ++length);
  }
  toArray() {
    let arr = [];
    this.prefixTraverce(node => arr.push(node.value));
    return arr;
  }
  toString() {
    
  }
}

const bst = new BinaryTree;
bst.add(2);
bst.add(5);
bst.add(3);
bst.add(8);
bst.add(0);
bst.add(1);
bst.add(-1);
bst.add(-5);
console.log(bst.contains(5));
console.log(bst.contains(-3));
console.log(bst.toArray());