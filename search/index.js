'use strict';

const { genRandArr, rand, executingTime, testSearch } = require('./help');
const binarySearch = require('./binary-search');
const interpolationSearch = require('./interpolation-search');

const times = 10000;

console.log(testSearch(binarySearch, times));
console.log(testSearch(interpolationSearch, times));

const executingStr = "Itnterpolation faster in: %dms";

const arraySize = 100000;
const arr = genRandArr(arraySize, 0, arraySize).sort((a, b) => a - b);
const numberToFind = '861998'.split('').reduce((sum, current) => sum * current);

const bTime = executingTime(binarySearch, arr, numberToFind);
const iTime = executingTime(interpolationSearch, arr, numberToFind);
console.log(executingStr, bTime - iTime);

