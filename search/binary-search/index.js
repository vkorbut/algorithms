'use strict';

function binarySearch(sortedArray, item) {
  if (!Array.isArray(sortedArray) || typeof item !== 'number') {
    throw new Error('Wrong input');
  }
  let minIndex = 0;
  let maxIndex = sortedArray.length - 1;
  let middle;
  let current;
  while (minIndex <= maxIndex) {
    middle = Math.floor((maxIndex + minIndex) / 2);
    current = sortedArray[middle];
    if (current === item) {
      return middle;
    }
    if (item < current) {
      maxIndex = middle - 1;
    }
    if (item > current) {
      minIndex = middle + 1;
    }
  }
  return -1;
}

module.exports = binarySearch;
