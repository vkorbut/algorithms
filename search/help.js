'use strict';

const now = require('performance-now');

const isArrayEqual = (arr1, arr2) => {
  if (arr1.length !== arr2.length) {
    return false;
  }
  for (let i = 0; i < arr1.length; ++i) {
    if (arr1[i] !== arr2[i]) {
      return false;
    }
  }
  return true;
}

const genRandArr = (n, min, max) => {
  let arr = [];
  for (let i = 0; i < n; ++i) {
    arr.push(rand(min, max));
  }
  return arr;
}

const rand = (min, max) => {
  if (max < min) {
    throw new Error('Wrong range');
  }
  return (Math.random() * (max - min + 1) + min) ^ 0;
}

const testSort = (cb, num) => {
  for (let i = 0; i < num; ++i) {
    let arr = genRandArr(1000, -100, 100);
    let arr1 = cb([...arr]);
    let arr2 = [...arr].sort((a, b) => a - b);
    if (!isArrayEqual(arr1, arr2)) {
      console.log('Not Passed');
      return false;
    }
  }
  console.log('Passed');
}

const testSearch = (searchCb, times) => {
  let errors = 0;
  for (let i = 1; i < times; ++i) {
    const arr = genRandArr(i, 0, i).sort((a, b) => a - b);
    const number = rand(0, i);
    const position = searchCb(arr, number);
    const positionIndex = arr.indexOf(number);
    const positionLastIndex = arr.lastIndexOf(number);
    if (position < positionIndex || positionIndex > positionLastIndex) {
      ++errors;
      break;
    }
  }
  return !errors;
}

const averegeExecutingTime = (fn, arrays) => {
  let times = [];
  for (let i = 0; i < arrays.length; ++i) {
    let arr = arrays.pop();
    times.push(executingTime(fn, arrays));
  }
  return times.reduce((sum, current) => sum + current) / times.length;
}

const executingTime = (fn, ...args) => {
  let start = now();
  fn(...args);
  return (now() - start).toFixed(5);
}


module.exports = {
  isArrayEqual,
  genRandArr,
  rand,
  testSort,
  averegeExecutingTime,
  executingTime,
  testSearch,
}