function hash(value) {
  return value % 103;
}

class HashTable {
  constructor() {
    this.values = {};
  }
  add(number) {
    let key = hash(number);
    let value = this.values[key];
    let arr;
    if (value && value !== 0) {
      if (typeof value === 'number') {
        if (value !== number) {
          arr = [value, number];
        }
      } else {
        if (!(value.indexOf(number) + 1)) {
          arr = [...value, number];
        }
      }
      if (arr) {
        this.values[key] = arr;
      }
    } else {
      this.values[key] = number;
    }
  }
  remove(number) {
    let key = hash(number);
    let value = this.values[key];
    if (typeof value === 'number') {
      this.values[key] = null;
    } else {
      let newValue = value.splice(value.indexOf(number), 1);
      this.values[key] = !(newValue.length - 1) ? newValue[0] : newValue;
    }
  }
  find(number) {
    let key = hash(number);
    let value = this.values[key];
    if(typeof value === 'number'){
      return value;
    }
    if(Array.isArray(value)){
      return value[value.indexOf(number)];
    }
    return null;
  }
}

let hashtable = new HashTable;
hashtable.add(5);
hashtable.add(108);
console.log(hashtable.find(108));
console.log(hashtable.find(6));