'use strict';


function isArrayEqual(arr1, arr2) {
  if (arr1.length !== arr2.length) {
    return false;
  }
  for (let i = 0; i < arr1.length; ++i) {
    if (arr1[i] !== arr2[i]) {
      return false;
    }
  }
  return true;
}

function genRandArr(n, min, max) {
  let arr = [];
  for (let i = 0; i < n; ++i) {
    arr.push(rand(min, max));
  }
  return arr;
}

function rand(min, max) {
  if (max < min) {
    throw new Error('Wrong range');
  }
  return (Math.random() * (max - min + 1) + min) ^ 0;
}

function testSort(cb, num) {
  for (let i = 0; i < num; ++i) {
    let arr = genRandArr(num, -num, num);
    let arr1 = cb([...arr]);
    let arr2 = [...arr].sort((a, b) => a - b);
    if (!isArrayEqual(arr1, arr2)) {
      console.log('Not Passed');
      return false;
    }
  }
  console.log('Passed');
}

function averegeExecutingTime(fn, arrays) {
  let times = [];
  let newarrays = [...arrays]
  while (newarrays.length) {
    let start = Date.now();
    fn(newarrays.pop());
    times.push(Date.now() - start);
  }
  return times.reduce((sum, current) => sum + current) / times.length;
}





function insertionSort(arr) {
  if (arr.length < 2) return arr;
  for (let i = 1; i < arr.length; ++i) {
    let current = arr[i];
    let j = i;
    while (arr[--j] > current) {
      arr[j + 1] = arr[j];
    }
    arr[j + 1] = current;
  }
  return arr;
}

function mergeSort(arr) {
  let len = arr.length;
  if (len < 2) return arr;
  let middle = (len / 2) ^ 0;
  return merge(
    mergeSort(arr.slice(0, middle)),
    mergeSort(arr.slice(middle, len))
  );
}

function mergeSortOptimized(arr) {
  let len = arr.length;
  if (len < 2) return arr;
  if (len < 16) {
    return insertionSort(arr);
  }
  let middle = (len / 2) ^ 0;
  return merge(
    mergeSortOptimized(arr.slice(0, middle)),
    mergeSortOptimized(arr.slice(middle, len))
  );
}

function merge(arr1, arr2) {
  let len1 = arr1.length;
  let len2 = arr2.length;
  let i1 = 0;
  let i2 = 0;
  let len = len1 + len2;
  let result = [];
  for (let i = 0; i < len; ++i) {
    if (i1 < len1 && i2 < len2) {
      if (arr1[i1] < arr2[i2]) {
        result[result.length] = arr1[i1++];
      } else {
        result[result.length] = arr2[i2++];
      }
    } else {
      if (i2 < len2) {
        result[result.length] = arr2[i2++];
      } else {
        result[result.length] = arr1[i1++];
      }
    }
  }
  return result;
}

function quickSort(arr) {
  const len = arr.length;
  if (!len) {
    return arr;
  }
  let middle = (len / 2) ^ 0;
  let pivot = arr.splice(middle, 1);
  let less = [];
  let greater = [];

  arr.forEach(function (el) {
    if (el <= pivot) {
      less.push(el);
    } else {
      greater.push(el);
    }
  });
  return quickSort(less).concat(pivot, quickSort(greater))
}

let arr50 = [];
let arr100 = [];
const times = 50;

for (let i = 0; i < times; ++i) {
  arr50.push(genRandArr(1000000, 0, 1000000));
}

for (let i = 0; i < times; ++i) {
  arr100.push(genRandArr(5000000, 0, 5000000));
}

function sort(arr) {
  return arr.sort((a, b) => a - b);
}

console.log(averegeExecutingTime(mergeSort, [...arr50]));
console.log(averegeExecutingTime(mergeSort, [...arr100]));

// console.log(averegeExecutingTime(quickSort, [...arr50]));
// console.log(averegeExecutingTime(quickSort, [...arr100]));

function sort(arr) {
  return arr.sort((a, b) => a - b);
}

console.log(averegeExecutingTime(mergeSortOptimized, [...arr50]));
console.log(averegeExecutingTime(mergeSortOptimized, [...arr100]));


console.log(averegeExecutingTime(sort, [...arr50]));
console.log(averegeExecutingTime(sort, [...arr100]));

// testSort(mergeSort, 10000);

