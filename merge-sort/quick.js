const genRandArr = (n, min, max) => {
  let arr = [];
  for (let i = 0; i < n; ++i) {
    arr.push(rand(min, max));
  }
  return arr;
}

const rand = (min, max) => {
  if (max < min) {
    throw new Error('Wrong range');
  }
  return (Math.random() * (max - min + 1) + min) ^ 0;
}

function quickSort(arr) {
  if (!arr.length) {
    return arr;
  }
  let middle = (arr.length / 2) ^ 0;
  let pivot = arr.splice(middle, 1);
  let less = [];
  let greater = [];

  arr.forEach(function (el) {
    if (el <= pivot) {
      less.push(el);
    } else {
      greater.push(el);
    }
  });

  return quickSort(less).concat(pivot, quickSort(greater))
}

console.log(quickSort(genRandArr(15, 1, 15)));